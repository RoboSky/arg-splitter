module ArgSplitter (split) where

import Data.Char 

data SplitCursor = SplitCursor { remainingSrc :: String, currentToken :: String, tokens :: [String] } 

splitQuote :: SplitCursor -> SplitCursor
splitQuote (SplitCursor ('"':str) t ts) = SplitCursor str "" ((reverse t):ts)
splitQuote (SplitCursor ('\\':c:str) t ts) = splitQuote $ SplitCursor str (c:t) ts
splitQuote (SplitCursor (c:str) t ts) = splitQuote $ SplitCursor str (c:t) ts

splitMain :: SplitCursor -> [String]
splitMain (SplitCursor "" "" ts) = reverse $ ts
splitMain (SplitCursor "" t ts) = reverse $ (reverse t):ts
splitMain (SplitCursor ('"':str) "" ts) = splitMain $ splitQuote $ SplitCursor str "" ts
splitMain (SplitCursor ('\\':c:str) t ts) = splitMain $ SplitCursor str (c:t) ts
splitMain (SplitCursor (c:str) "" ts) | isSeparator c = splitMain $ SplitCursor str "" ts
splitMain (SplitCursor (c:str) t ts)  | isSeparator c = splitMain $ SplitCursor str "" ((reverse t):ts)
                                      | otherwise     = splitMain $ SplitCursor str (c:t) ts

split :: String -> [String]
split str = splitMain $ SplitCursor str "" []
