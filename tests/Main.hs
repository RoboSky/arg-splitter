import Test.Hspec
import ArgSplitter

main = hspec $ do
    context "when the input has no quotes" $
        it "returns a list of every word in the string" $
            let x = "I have had it with these motherf****n' snakes on this motherf****n' plane."
             in split x `shouldBe` words x

    context "when the entire input has quotes around it" $
        it "returns the input as the only element of a list" $
            let x = "I have had it with these motherf****n' snakes on this motherf****n' plane."
             in split ("\"" ++ x ++ "\"") `shouldBe` [x]

    context "when the input is split into two arguments, with quotes" $
        it "returns each argument while respecting quotes" $
             split "\"Motherf****n' snakes\" \"motherf****n' plane\""
                `shouldBe` ["Motherf****n' snakes", "motherf****n' plane"]

    context "when there are quotes, but no spaces" $
        it "returns a list of every word in the string, as if there were no quotes" $
            split " \"Snakes\" \"on\" \"a\" \"plane\"" `shouldBe` ["Snakes", "on", "a", "plane"]
